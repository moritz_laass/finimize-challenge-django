import request from "axios";

export const calculate = (savingsAmount, interestRateInPercent) => {
  return request.post("/calculate/", {
    savingsAmount,
    interestRateInPercent
  });
};
