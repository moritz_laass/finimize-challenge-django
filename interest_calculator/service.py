import decimal
from decimal import Decimal as D

def calculateInterestStep(amount, interest_rate_in_percent):
    interest_rate =  D(interest_rate_in_percent)/D(100.00)
    interest = amount* interest_rate
    return  amount + interest,  interest

def getIntervalMod(interest_interval):
    switcher = {'y': 12, 'q': 3, 'm': 1}
    return int(switcher.get(interest_interval, 0))

def accumulateMonthly(month, previous, recurrent_deposit, interval_mod, interest_rate_in_percent):
    accumulated = D(previous) + D(recurrent_deposit)
    interest = 0
    if int(month) % interval_mod == 0:
        accumulated, interest = calculateInterestStep(accumulated, interest_rate_in_percent)

    return accumulated, interest

def calculateSavings(savings_amount, recurrent_deposit, interest_interval, interest_rate_in_percent, time_in_months = 12 * 50):
    decimal.getcontext().prec = 16
    interval_mod = getIntervalMod(interest_interval)

    accumulated = D(savings_amount)
    savings = [{'month': 0, 'accumulated': accumulated, 'interest': 0}]

    for month in range(1, time_in_months+1):
        accumulated, interest = accumulateMonthly(month, accumulated, recurrent_deposit, interval_mod, interest_rate_in_percent)
        saving = {'month': month, 'accumulated': accumulated, 'interest': interest}
        savings.append(saving)
        #print( accumulated)

    return {
        'timeInMonths': time_in_months,
        'savingsMonthly': savings ,
        'finalSavings': accumulated
    }