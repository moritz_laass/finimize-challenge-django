from django.http import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt

from .service import calculateSavings


import json

@require_POST
@csrf_exempt
def calculate(request):
    params = json.loads(request.body)
    savings_amount = params.get('savingsAmount', None)
    recurrent_deposit = params.get('recurrentDeposit', None)
    interest_interval = params.get('interesInterval', None)
    interest_rate_in_percent = float(params.get('interestRateInPercent', None))
    time_in_months = int(params.get('timeInMonths', 50*12))

    if savings_amount is None or recurrent_deposit is None or interest_interval is None or interest_rate_in_percent is None:
        return HttpResponseBadRequest('Required parameters are not provided')

    if interest_interval not in {'y', 'q', 'm'} :
        return HttpResponseBadRequest('interestInterval must be "y": yearly, "q": quarterly or "m": monthly')


    result = calculateSavings(savings_amount, recurrent_deposit, interest_interval, interest_rate_in_percent, time_in_months)
    return JsonResponse(result)